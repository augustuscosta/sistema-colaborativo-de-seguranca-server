class AlarmesController < ApplicationController
  #before_filter :authenticate_usuario!
  # GET /alarmes
  # GET /alarmes.json
  def index
    @alarmes = Alarme.listGroupsAlarms(current_usuario)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @alarmes.to_json(:include => [:sensor => {:include => [:tipo_sensor, :usuario]}]) }
    end
  end

  # GET /alarmes/1
  # GET /alarmes/1.json
  def show
    @alarme = Alarme.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @alarme.to_json(:include => [:atendimentos, :comentarios ,:sensor => 
        {:include => [:tipo_sensor, :usuario => 
          {:include => [:grupo_colaborativos => {:include => [:usuarios]}]}]}]) }
    end
  end

  # GET /alarmes/new
  # GET /alarmes/new.json
  def new
    @alarme = Alarme.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @alarme }
    end
  end

  # GET /alarmes/1/edit
  def edit
    @alarme = Alarme.find(params[:id])
  end

  # POST /alarmes
  # POST /alarmes.json
  def create
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        @alarme = Alarme.new(params[:alarme])
        if @alarme.save
          send_notification(@alarme)
          redirect_to @alarme, notice: 'Alarme was successfully created.' 
        else
          render action: "new"
        end
      }
      format.json {
        logger.info "From JSON"
        @alarme = Alarme.new
        @alarme.attributes = ActiveSupport::JSON.decode(params[:alarme])
        if @alarme.save
         send_notification(@alarme)
         render json: @alarme, status: :created, location: @alarme
        else
          render json: @alarme.errors, status: :unprocessable_entity
        end
      }
    end
  end

  # PUT /alarmes/1
  # PUT /alarmes/1.json
  def update
    @alarme = Alarme.find(params[:id])

    respond_to do |format|
      if @alarme.update_attributes(params[:alarme])
        format.html { redirect_to @alarme, notice: 'Alarme was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @alarme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alarmes/1
  # DELETE /alarmes/1.json
  def destroy
    @alarme = Alarme.find(params[:id])
    @alarme.destroy

    respond_to do |format|
      format.html { redirect_to alarmes_url }
      format.json { head :no_content }
    end
  end

  def send_notification(alarme)
    prepare_sensor_config(alarme)
    usuario = alarme.sensor.usuario
    registration_ids = Array.new
    registration_ids << usuario.device
    unless usuario.grupo_colaborativos.nil?
      usuario.grupo_colaborativos.each do |group|
        group.usuarios.each do |user|
          unless usuario.id == user.id
            unless user.device.nil?
              create_notification(user.device, alarme)
            end
          end
        end
      end
    end
    send_stored_notifications()
  end

  def prepare_sensor_config(alarme)
     if alarme.sensor.nil?
      sensor_code = "Celular" + "_" + current_usuario.id.to_s
        @sensor = current_usuario.sensors.find(:first, :conditions => [ "codigo = ?", sensor_code])
        if @sensor.nil?

          @tipo = TipoSensor.find(:first, :conditions => [ "nome = ?", "Celular"])
          if @tipo.nil?
            @tipo = TipoSensor.new
            @tipo.nome = "Celular"
            @tipo.descricao = "Dispositivo"
            @tipo.save
          end

          @sensor = Sensor.new
          @sensor.codigo = sensor_code
          @sensor.status = "Ativo"
          @sensor.tipo_sensor = @tipo
          @sensor.usuario = current_usuario
          @sensor.localizacao = "Celular"
          @sensor.save
        end
        alarme.sensor = @sensor
        alarme.save
      end
   end

  def create_notification(registration_id, alarme)
    json = alarme.to_json(:include => [:comentarios , :sensor => 
        {:include => [:tipo_sensor, :usuario => 
          {:include => [:grupo_colaborativos => {:include => [:usuarios]}]}]}])
    logger.info(json)
    device = Gcm::Device.where(:registration_id => registration_id).first
    if device.nil?
      device = Gcm::Device.create(:registration_id => registration_id)
    end
    notification = Gcm::Notification.new
    notification.device = device
    notification.collapse_key = "alarme_criado"
    notification.delay_while_idle = true
    #notification.data = {:registration_ids => registration_id, :data => alarme.to_json(:include => [:sensor => {:include => :tipo_sensor}])}
    notification.data = {:registration_ids => [registration_id], :data => {:message_text => json}}
    notification.save
  end

  def send_stored_notifications()
    Gcm::Notification.send_notifications
  end

end
