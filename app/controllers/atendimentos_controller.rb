class AtendimentosController < ApplicationController
  before_filter :authenticate_usuario!
  # GET /atendimentos
  # GET /atendimentos.json
  def index
    @atendimentos = Atendimento.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @atendimentos }
    end
  end

  # GET /atendimentos/1
  # GET /atendimentos/1.json
  def show
    @atendimento = Atendimento.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @atendimento }
    end
  end

  # GET /atendimentos/new
  # GET /atendimentos/new.json
  def new
    @atendimento = Atendimento.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @atendimento }
    end
  end

  # GET /atendimentos/1/edit
  def edit
    @atendimento = Atendimento.find(params[:id])
  end

  # POST /atendimentos
  # POST /atendimentos.json
  def create
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        @atendimento = Atendimento.new(params[:atendimento])
        @atendimento.usuario = current_usuario
        if @atendimento.save
          redirect_to @atendimento, notice: 'Atendimento was successfully created.' 
        else
          render action: "new"
        end
      }
      format.json {
        logger.info "From JSON"
        @atendimento = Atendimento.new
        @atendimento.attributes = ActiveSupport::JSON.decode(params[:atendimento])
        @atendimento.usuario = current_usuario
        if @atendimento.save
         render json: @atendimento.to_json(:include => [:usuario]), status: :created, location: @atendimento
        else
          render json: @atendimento.errors, status: :unprocessable_entity
        end
      }
    end
  end

  # PUT /atendimentos/1
  # PUT /atendimentos/1.json
  def update
    @atendimento = Atendimento.find(params[:id])
    logger.info params[:id]
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        if @atendimento.update_attributes(params[:atendimento])
          redirect_to @atendimento, notice: 'Atendimento was successfully update.' 
        else
          render action: "edit"
        end
      }
      format.json {
        logger.info "From JSON"
        @atendimento.attributes = ActiveSupport::JSON.decode(params[:atendimento])
        if @atendimento.save
         render json: @atendimento.to_json(:include => [:usuario]), status: :created, location: @atendimento
        else
          render json: @atendimento.errors, status: :unprocessable_entity
        end
      }
    end
  end

  # DELETE /atendimentos/1
  # DELETE /atendimentos/1.json
  def destroy
    @atendimento = Atendimento.find(params[:id])
    @atendimento.destroy

    respond_to do |format|
      format.html { redirect_to atendimentos_url }
      format.json { head :no_content }
    end
  end
end
