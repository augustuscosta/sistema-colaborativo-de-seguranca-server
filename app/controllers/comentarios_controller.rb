class ComentariosController < ApplicationController
  before_filter :authenticate_usuario!
  # GET /comentarios
  # GET /comentarios.json
  def index
    @comentarios = Comentario.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comentarios }
    end
  end

  # GET /comentarios/1
  # GET /comentarios/1.json
  def show
    @comentario = Comentario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comentario }
    end
  end

  # GET /comentarios/new
  # GET /comentarios/new.json
  def new
    @comentario = Comentario.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comentario }
    end
  end

  # GET /comentarios/1/edit
  def edit
    @comentario = Comentario.find(params[:id])
  end

  # POST /comentarios
  # POST /comentarios.json
  def create
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        @comentario = Comentario.new(params[:comentario])
        @comentario.usuario = current_usuario
        if @comentario.save
          redirect_to @comentario, notice: 'Comentario was successfully created.' 
        else
          render action: "new"
        end
      }
      format.json {
        logger.info "From JSON"
        @comentario = Comentario.new
        @comentario.attributes = ActiveSupport::JSON.decode(params[:comentario])
        @comentario.usuario = current_usuario
        if @comentario.save
         render json: @comentario.to_json(:include => [:usuario]), status: :created, location: @comentario
        else
          render json: @comentario.errors, status: :unprocessable_entity
        end
      }
    end
  end

  # PUT /comentarios/1
  # PUT /comentarios/1.json
  def update
    @comentario = Comentario.find(params[:id])

    respond_to do |format|
      if @comentario.update_attributes(params[:comentario])
        format.html { redirect_to @comentario, notice: 'Comentario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comentario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comentarios/1
  # DELETE /comentarios/1.json
  def destroy
    @comentario = Comentario.find(params[:id])
    @comentario.destroy

    respond_to do |format|
      format.html { redirect_to comentarios_url }
      format.json { head :no_content }
    end
  end
end
