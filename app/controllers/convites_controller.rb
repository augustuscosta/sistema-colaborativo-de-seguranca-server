class ConvitesController < ApplicationController
  before_filter :authenticate_usuario!
  # GET /convites
  # GET /convites.json
  def index
    logger.info current_usuario.email
    @convites = Convite.where(:email_destinatario => current_usuario.email)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @convites }
    end
  end

  # GET /convites/1
  # GET /convites/1.json
  def show
    @convite = Convite.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @convite }
    end
  end

  # GET /convites/new
  # GET /convites/new.json
  def new
    @convite = Convite.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @convite }
    end
  end

  # GET /convites/1/edit
  def edit
    @convite = Convite.find(params[:id])
  end

  # POST /convites
  # POST /convites.json
  def create
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        @convite = Convite.new(params[:convite])
        if @convite.save
          redirect_to @convite, notice: 'Convite was successfully created.' 
        else
          render action: "new"
        end
      }
      format.json {
        logger.info "From JSON"
        @convite = Convite.new
        @convite.attributes = ActiveSupport::JSON.decode(params[:convite])
        addUserInfoToConvite(@convite)
        if @convite.save
         ConviteMailer.send_convite(@convite).deliver  
         render json: @convite, status: :created, location: @convite
        else
          render json: @convite.errors, status: :unprocessable_entity
        end
      }
    end
  end

  def addUserInfoToConvite(convite)
    convite.email_remetente = current_usuario.email
    convite.nome_remetente = current_usuario.name
  end

  # PUT /convites/1
  # PUT /convites/1.json
  def update
    @convite = Convite.find(params[:id])

    grupo = GrupoColaborativo.find(@convite.grupo_id)
    usuario = Usuario.where(:email => @convite.email_destinatario)
    if grupo.usuarios.find_by_id(usuario).nil?
      logger.info "Usuário vai ser inscrito no grupo"
      grupo.usuarios << usuario
      grupo.update_attributes(:email_dono => grupo.email_dono)
    end
    @convite.destroy

    respond_to do |format|
      format.html { redirect_to convites_url }
      format.json { head :no_content }
    end
  end

  # DELETE /convites/1
  # DELETE /convites/1.json
  def destroy
    @convite = Convite.find(params[:id])
    @convite.destroy

    respond_to do |format|
      format.html { redirect_to convites_url }
      format.json { head :no_content }
    end
  end
end
