class GrupoColaborativosController < ApplicationController
  before_filter :authenticate_usuario!
  # GET /grupo_colaborativos
  # GET /grupo_colaborativos.json
  def index
    @grupo_colaborativos = current_usuario.grupo_colaborativos

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @grupo_colaborativos.to_json(:include => [:usuarios]) }
    end
  end

  # GET /grupo_colaborativos/1
  # GET /grupo_colaborativos/1.json
  def show
    @grupo_colaborativo = GrupoColaborativo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @grupo_colaborativo.to_json(:include => [:usuarios]) }
    end
  end

  # GET /grupo_colaborativos/new
  # GET /grupo_colaborativos/new.json
  def new
    @grupo_colaborativo = GrupoColaborativo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @grupo_colaborativo }
    end
  end

  # GET /grupo_colaborativos/1/edit
  def edit
    @grupo_colaborativo = GrupoColaborativo.find(params[:id])
  end

  def addUserToGroup(group)
    group.usuarios = Array.new 
    group.usuarios << current_usuario
  end

  # POST /grupo_colaborativos
  # POST /grupo_colaborativos.json
  def create
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        @grupo_colaborativo = GrupoColaborativo.new(params[:grupo_colaborativo])
        addUserToGroup(@grupo_colaborativo)
        if @grupo_colaborativo.save
          redirect_to @grupo_colaborativo, notice: 'Grupo was successfully created.' 
        else
          render action: "new"
        end
      }
      format.json {
        logger.info "From JSON"
        @grupo_colaborativo = GrupoColaborativo.new
        @grupo_colaborativo.attributes = ActiveSupport::JSON.decode(params[:grupo_colaborativo])
        addUserToGroup(@grupo_colaborativo)
        if @grupo_colaborativo.save
         render json: @grupo_colaborativo, status: :created, location: @grupo_colaborativo
        else
          render json: @grupo_colaborativo.errors, status: :unprocessable_entity
        end
      }
    end
  end

  # PUT /grupo_colaborativos/1
  # PUT /grupo_colaborativos/1.json
  def update
    @grupo_colaborativo = GrupoColaborativo.find(params[:id])

    respond_to do |format|
      if @grupo_colaborativo.update_attributes(params[:grupo_colaborativo])
        format.html { redirect_to @grupo_colaborativo, notice: 'Grupo colaborativo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @grupo_colaborativo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grupo_colaborativos/1
  # DELETE /grupo_colaborativos/1.json
  def destroy
    @grupo_colaborativo = GrupoColaborativo.find(params[:id])
    if @grupo_colaborativo.usuarios.blank?
      logger.info "Grupo sem usuários -> Remover"
      @grupo_colaborativo.destroy
    else
      if @grupo_colaborativo.usuarios.size == 1
        logger.info "Grupo com 1 usuário -> Remover"
        @grupo_colaborativo.destroy
      else
        logger.info "Grupo com N usuários -> Remover apenas o usuário"
        @grupo_colaborativo.usuarios.delete(current_usuario)
      end
    end
    respond_to do |format|
      format.html { redirect_to grupo_colaborativos_url }
      format.json { head :no_content }
    end
  end

  def remove_usuario
    
    grupo = GrupoColaborativo.new
    grupo = ActiveSupport::JSON.decode(params[:grupo_colaborativo])
    grupo = GrupoColaborativo.find(grupo['id'])
    

    usuario = Usuario.new
    usuario = ActiveSupport::JSON.decode(params[:usuario]) 
    usuario = Usuario.find(usuario['id'])
    grupo.usuarios.delete(usuario)
    
    respond_to do |format|
      format.html { redirect_to grupo_colaborativos_url }
      format.json { head :no_content }
    end
  end
end
