class TipoSensorsController < ApplicationController
  before_filter :authenticate_usuario!
  # GET /tipo_sensors
  # GET /tipo_sensors.json
  def index
    @tipo_sensors = TipoSensor.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tipo_sensors }
    end
  end

  # GET /tipo_sensors/1
  # GET /tipo_sensors/1.json
  def show
    @tipo_sensor = TipoSensor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tipo_sensor }
    end
  end

  # GET /tipo_sensors/new
  # GET /tipo_sensors/new.json
  def new
    @tipo_sensor = TipoSensor.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tipo_sensor }
    end
  end

  # GET /tipo_sensors/1/edit
  def edit
    @tipo_sensor = TipoSensor.find(params[:id])
  end

  # POST /tipo_sensors
  # POST /tipo_sensors.json
  def create
    @tipo_sensor = TipoSensor.new(params[:tipo_sensor])

    respond_to do |format|
      if @tipo_sensor.save
        format.html { redirect_to @tipo_sensor, notice: 'Tipo sensor was successfully created.' }
        format.json { render json: @tipo_sensor, status: :created, location: @tipo_sensor }
      else
        format.html { render action: "new" }
        format.json { render json: @tipo_sensor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tipo_sensors/1
  # PUT /tipo_sensors/1.json
  def update
    @tipo_sensor = TipoSensor.find(params[:id])

    respond_to do |format|
      if @tipo_sensor.update_attributes(params[:tipo_sensor])
        format.html { redirect_to @tipo_sensor, notice: 'Tipo sensor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tipo_sensor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_sensors/1
  # DELETE /tipo_sensors/1.json
  def destroy
    @tipo_sensor = TipoSensor.find(params[:id])
    @tipo_sensor.destroy

    respond_to do |format|
      format.html { redirect_to tipo_sensors_url }
      format.json { head :no_content }
    end
  end
end
