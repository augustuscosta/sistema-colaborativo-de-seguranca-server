class UsuariosController < ApplicationController
  before_filter :authenticate_usuario!
  # GET /usuarios
  # GET /usuarios.json
  def index
    @usuarios = Usuario.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @usuarios }
    end
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/new
  # GET /usuarios/new.json
  def new
    @usuario = Usuario.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/1/edit
  def edit
    @usuario = Usuario.find(params[:id])
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    respond_to do |format|
      format.html { 
        logger.info "From HTML"
        @usuario = Usuario.new(params[:usuario])
        if @usuario.save
          redirect_to @usuario, notice: 'Usuário was successfully created.' 
        else
          render action: "new"
        end
      }
      format.json {
        logger.info "From JSON"
        @usuario = Usuario.new
        @usuario.attributes = ActiveSupport::JSON.decode(params[:usuario])
        if @usuario.save
         render json: @usuario, status: :created, location: @usuario
        else
          render json: @usuario.errors, status: :unprocessable_entity
        end
      }
    end
  end

  # PUT /usuarios/1
  # PUT /usuarios/1.json
  def update
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      if @usuario.update_attributes(params[:usuario])
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  def register_device
    @usuario = current_usuario

    respond_to do |format|
      if @usuario.update_attribute('device',params[:device])
        format.html { render json: @usuario, status: :ok }
        format.json { render json: @usuario, status: :ok }
      else
        format.html { render json: @usuario.errors, status: :unprocessable_entity }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  def unregister_device
    @usuario = current_usuario

    respond_to do |format|
      if @usuario.update_attribute('device',nil)
        format.html { render json: @usuario, status: :ok }
        format.json { render json: @usuario, status: :ok }
      else
        format.html { render json: @usuario.errors, status: :unprocessable_entity }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario = Usuario.find(params[:id])
    @usuario.destroy

    respond_to do |format|
      format.html { redirect_to usuarios_url }
      format.json { head :no_content }
    end
  end
end
