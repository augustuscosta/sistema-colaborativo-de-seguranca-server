class ConviteMailer < ActionMailer::Base
  default from: "segurancacolaborativa@gmail.com"
  def send_convite(convite)  
  	@convite = convite 
    mail(:to => convite.email_destinatario, :subject => "Convite")  
  end 
end
