class Alarme < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :data, :status, :tempo_atendimento, :sensor_id, :latitude, :longitude
  belongs_to :sensor
  has_many :comentarios
  has_many :atendimentos


  def self.listGroupsAlarms(usuario)
  	Alarme.joins(:sensor).where(:sensors => {:usuario_id => getIdsUsuariosDosGrupos(usuario)})
  end

  def self.getIdsUsuariosDosGrupos(usuario)
  	users_ids = Array.new

  	unless usuario.grupo_colaborativos.blank?
  		
  		usuario.grupo_colaborativos.each do |grupo|
  			
  			unless grupo.usuarios.blank?
  				
  				grupo.usuarios.each do |usuario|
  					
  					unless users_ids.include?(usuario.id)
   						
              users_ids << usuario.id
					  end
  				end
  			end
  		end
  	end

  	users_ids
  end

end
