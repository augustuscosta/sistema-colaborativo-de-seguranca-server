class Atendimento < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :hora_encerramento, :hora_inicio, :status, :usuario_id, :alarme_id
  belongs_to :usuario
  belongs_to :alarme
end
