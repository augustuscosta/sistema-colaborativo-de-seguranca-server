class Comentario < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :alarme_id, :data, :mensagem, :usuario_id
  belongs_to :usuario
  belongs_to :alarme
end
