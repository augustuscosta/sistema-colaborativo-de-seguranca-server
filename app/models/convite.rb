class Convite < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :email_destinatario, :email_remetente, :grupo_id, :grupo_nome, :nome_remetente
end
