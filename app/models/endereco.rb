class Endereco < ActiveRecord::Base
  attr_accessible :bairro, :cep, :cidade, :estado, :latitude, :logradouro, :longitude, :numero, :referencia
  has_many :usuarios
end
