class GrupoColaborativo < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :descricao, :nome, :email_dono
  has_and_belongs_to_many :usuarios
end
