class Sensor < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :codigo, :localizacao, :status, :latitude, :longitude
  belongs_to :usuario
  belongs_to :tipo_sensor
  has_many :alarmes

  validates_presence_of :codigo
  validates_uniqueness_of :codigo
end
