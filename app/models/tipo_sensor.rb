class TipoSensor < ActiveRecord::Base
  self.include_root_in_json = true
  attr_accessible :descricao, :nome
  has_many :sensors
end
