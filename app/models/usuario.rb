class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :nascimento, :sexo, :telefone, :celular, :cpf_cnpj, :tipo, :device
  # attr_accessible :title, :body

  belongs_to :endereco
  has_many :sensors
  has_many :atendimentos
  has_many :comentarios
  has_and_belongs_to_many :grupo_colaborativos
end
