class CreateTipoSensors < ActiveRecord::Migration
  def change
    create_table :tipo_sensors do |t|
      t.string :nome
      t.string :descricao

      t.timestamps
    end
  end
end
