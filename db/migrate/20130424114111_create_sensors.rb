class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :codigo
      t.string :localizacao
      t.string :status
      t.integer :tipo_sensor_id
      t.integer :usuario_id
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
