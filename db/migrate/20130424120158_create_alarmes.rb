class CreateAlarmes < ActiveRecord::Migration
  def change
    create_table :alarmes do |t|
      t.datetime :data
      t.integer :tempo_atendimento
      t.string :status
      t.integer :sensor_id
      t.float :latitude
      t.float :longitude
      t.string :descricao

      t.timestamps
    end
  end
end
