class CreateGrupoColaborativos < ActiveRecord::Migration
  def change
    create_table :grupo_colaborativos do |t|
      t.string :nome
      t.string :descricao
      t.string :email_dono

      t.timestamps
    end
  end
end
