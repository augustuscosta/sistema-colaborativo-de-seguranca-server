class AddUsuariosGrupoColaborativosJoinTable < ActiveRecord::Migration
  def up
  	create_table :grupo_colaborativos_usuarios, :id => false do |t|
      t.integer :usuario_id
      t.integer :grupo_colaborativo_id
    end
  end

  def down
  	drop_table :usuarios_grupo_colaborativos
  end
end
