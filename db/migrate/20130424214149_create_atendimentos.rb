class CreateAtendimentos < ActiveRecord::Migration
  def change
    create_table :atendimentos do |t|
      t.string :status
      t.datetime :hora_inicio
      t.datetime :hora_encerramento
      t.integer :usuario_id
      t.integer :alarme_id

      t.timestamps
    end
  end
end
