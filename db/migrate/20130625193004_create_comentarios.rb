class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.string :mensagem
      t.integer :usuario_id
      t.integer :alarme_id
      t.datetime :data

      t.timestamps
    end
  end
end
