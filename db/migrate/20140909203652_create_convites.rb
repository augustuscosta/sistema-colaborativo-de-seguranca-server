class CreateConvites < ActiveRecord::Migration
  def change
    create_table :convites do |t|
      t.string :email_remetente
      t.string :email_destinatario
      t.integer :grupo_id
      t.string :grupo_nome
      t.string :nome_remetente

      t.timestamps
    end
  end
end
