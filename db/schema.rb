# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140909203652) do

  create_table "alarmes", :force => true do |t|
    t.datetime "data"
    t.integer  "tempo_atendimento"
    t.string   "status"
    t.integer  "sensor_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "descricao"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "atendimentos", :force => true do |t|
    t.string   "status"
    t.datetime "hora_inicio"
    t.datetime "hora_encerramento"
    t.integer  "usuario_id"
    t.integer  "alarme_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "comentarios", :force => true do |t|
    t.string   "mensagem"
    t.integer  "usuario_id"
    t.integer  "alarme_id"
    t.datetime "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "convites", :force => true do |t|
    t.string   "email_remetente"
    t.string   "email_destinatario"
    t.integer  "grupo_id"
    t.string   "grupo_nome"
    t.string   "nome_remetente"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "enderecos", :force => true do |t|
    t.string   "logradouro"
    t.string   "numero"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "estado"
    t.string   "cep"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "referencia"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "gcm_devices", :force => true do |t|
    t.string   "registration_id",    :null => false
    t.datetime "last_registered_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "gcm_devices", ["registration_id"], :name => "index_gcm_devices_on_registration_id", :unique => true

  create_table "gcm_notifications", :force => true do |t|
    t.integer  "device_id",        :null => false
    t.string   "collapse_key"
    t.text     "data"
    t.boolean  "delay_while_idle"
    t.datetime "sent_at"
    t.integer  "time_to_live"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "gcm_notifications", ["device_id"], :name => "index_gcm_notifications_on_device_id"

  create_table "grupo_colaborativos", :force => true do |t|
    t.string   "nome"
    t.string   "descricao"
    t.string   "email_dono"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "grupo_colaborativos_usuarios", :id => false, :force => true do |t|
    t.integer "usuario_id"
    t.integer "grupo_colaborativo_id"
  end

  create_table "sensors", :force => true do |t|
    t.string   "codigo"
    t.string   "localizacao"
    t.string   "status"
    t.integer  "tipo_sensor_id"
    t.integer  "usuario_id"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "tipo_sensors", :force => true do |t|
    t.string   "nome"
    t.string   "descricao"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "usuarios", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.date     "nascimento"
    t.string   "sexo"
    t.string   "telefone"
    t.string   "celular"
    t.string   "cpf_cnpj"
    t.string   "tipo"
    t.integer  "endereco_id"
    t.string   "device"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "usuarios", ["email"], :name => "index_usuarios_on_email", :unique => true
  add_index "usuarios", ["reset_password_token"], :name => "index_usuarios_on_reset_password_token", :unique => true

end
