require 'rake'
class NotificationManager
	
	def send_notification(alarme)
		usuario = alarme.sensor.usuario
		json = alarme.to_json(:include => [:sensor => {:include => :tipo_sensor}])
		registration_ids = Array.new
		registration_ids << usuario.device
		unless usuario.grupos.nil?
			usuario.grupos.each do |group|
				group.usuarios.each do |user|
					unless usuario.id == user.id
						registration_ids << user.device
					end
				end
			end
		end
		create_notification(registration_ids, json)
		send_stored_notifications()
	end

	def create_notification(registration_ids, json)
		notification = Gcm::Notification.new
		#notification.device = device
		#notification.collapse_key = "updates_available"
		notification.delay_while_idle = true
		notification.data = {:registration_ids => registration_ids, :data => {:message_text => json}}
		notification.save
	end

	def send_stored_notifications()
		Rake::Task['gcm:notifications:deliver'].invoke
	end

end
