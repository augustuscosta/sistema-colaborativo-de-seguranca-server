require 'test_helper'

class GrupoColaborativosControllerTest < ActionController::TestCase
  setup do
    @grupo_colaborativo = grupo_colaborativos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:grupo_colaborativos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create grupo_colaborativo" do
    assert_difference('GrupoColaborativo.count') do
      post :create, grupo_colaborativo: { descricao: @grupo_colaborativo.descricao, nome: @grupo_colaborativo.nome }
    end

    assert_redirected_to grupo_colaborativo_path(assigns(:grupo_colaborativo))
  end

  test "should show grupo_colaborativo" do
    get :show, id: @grupo_colaborativo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @grupo_colaborativo
    assert_response :success
  end

  test "should update grupo_colaborativo" do
    put :update, id: @grupo_colaborativo, grupo_colaborativo: { descricao: @grupo_colaborativo.descricao, nome: @grupo_colaborativo.nome }
    assert_redirected_to grupo_colaborativo_path(assigns(:grupo_colaborativo))
  end

  test "should destroy grupo_colaborativo" do
    assert_difference('GrupoColaborativo.count', -1) do
      delete :destroy, id: @grupo_colaborativo
    end

    assert_redirected_to grupo_colaborativos_path
  end
end
