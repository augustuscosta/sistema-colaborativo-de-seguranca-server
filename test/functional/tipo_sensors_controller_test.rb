require 'test_helper'

class TipoSensorsControllerTest < ActionController::TestCase
  setup do
    @tipo_sensor = tipo_sensors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_sensors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_sensor" do
    assert_difference('TipoSensor.count') do
      post :create, tipo_sensor: { descricao: @tipo_sensor.descricao, nome: @tipo_sensor.nome }
    end

    assert_redirected_to tipo_sensor_path(assigns(:tipo_sensor))
  end

  test "should show tipo_sensor" do
    get :show, id: @tipo_sensor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_sensor
    assert_response :success
  end

  test "should update tipo_sensor" do
    put :update, id: @tipo_sensor, tipo_sensor: { descricao: @tipo_sensor.descricao, nome: @tipo_sensor.nome }
    assert_redirected_to tipo_sensor_path(assigns(:tipo_sensor))
  end

  test "should destroy tipo_sensor" do
    assert_difference('TipoSensor.count', -1) do
      delete :destroy, id: @tipo_sensor
    end

    assert_redirected_to tipo_sensors_path
  end
end
